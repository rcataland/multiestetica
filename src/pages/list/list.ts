import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  title: string;

  constructor(public navParams: NavParams) {
    this.title = navParams.get('title');
  }
}
