import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Content, NavParams } from 'ionic-angular';
import { TreatmentModel } from '../../models/treatment.model';

@Component({
  selector: 'page-treatment',
  templateUrl: 'treatment.html',
})
export class TreatmentPage {
  // Public properties
  public title: string;
  public searchValue: string;
  public bodyData: Array<TreatmentModel>;
  public faceAndNeckData: Array<TreatmentModel>;
  public filteredTreatments: Array<TreatmentModel>;
  public showTreatment: Object = {
    faceAndNeck: true,
    body: true
  }
  @ViewChild(Content) content: Content;
  
  constructor(public params: NavParams, private _http: HttpClient) {
    this.title = this.params.get('title');

    // Load medical treatments json files
    this._http.get("assets/json/medical-treatment/body.json")
      .subscribe((data: Array<TreatmentModel>) => {
        this.bodyData = data;
      });
    this._http.get("assets/json/medical-treatment/face-and-neck.json")
      .subscribe((data: Array<TreatmentModel>) => {
        this.faceAndNeckData = data;
      });
  }

  public onInut (event: any): Array<TreatmentModel>  {
    try {
      // set val to the value of the ev target
      this.searchValue = event.target.value;

      // if the value is an empty string don't filter the items
			if (this.searchValue && this.searchValue.trim() != '') {
				this.filteredTreatments = this.bodyData.concat(this.faceAndNeckData).filter((treatment) => {
					return treatment.name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1
				})
			}
			return [];
      
    } catch (error) {
      console.error(error);
    }
  }

  public onCancel (): void {
    this.filteredTreatments = [];
  }

  public toggleTreatment(treatment: string): void {
    this.showTreatment[treatment] = !this.showTreatment[treatment];
  }

  public toggleFavorite (treatment): void {
    treatment.favorite = !treatment.favorite;
  }

  public hasFavorites (): boolean {
    let treatment: TreatmentModel;
    if (this.bodyData && this.faceAndNeckData){
      treatment = this.bodyData.concat(this.faceAndNeckData).find(treatment => {
        return treatment && treatment.favorite;
      });
    }
    return treatment && treatment.favorite;
  }

  public getFavoriteTreatments (): Array<TreatmentModel> {
    return this.bodyData.concat(this.faceAndNeckData).filter(treatment => {
      return treatment.favorite;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TreatmentPage');
  }
}
