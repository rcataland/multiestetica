import { Component, Input } from '@angular/core';
import { TreatmentModel } from '../../models/treatment.model';

@Component({
    selector:    'treatment',
	templateUrl: 'treatment.component.html'
})
export class Treatment {
    @Input('data') treatment: TreatmentModel;

    constructor () {}

    public toggleFavorite (treatment): void {
        treatment.favorite = !treatment.favorite;
    }
}
