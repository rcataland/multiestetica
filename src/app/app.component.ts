import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TreatmentPage } from '../pages/treatment/treatment';
import { ListPage } from '../pages/list/list';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = TreatmentPage;
  currentPage: string

  pages: Array<{title: string, component: any, active: boolean}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Experiencias', component: ListPage, active: false },
      { title: 'Tratamientos', component: TreatmentPage, active: true },
      { title: 'Doctores', component: ListPage, active: false },
      { title: 'Ofertas', component: ListPage, active: false },
      { title: 'Pregunta al doctor', component: ListPage, active: false },
      { title: 'Foro', component: ListPage, active: false }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component, { title: page.title });
    this.setCurrentPage(page.title);
  }

  private setCurrentPage (title: string): void {
    for (let i = 0; i < this.pages.length; i++) {
        this.pages[i].active = false;
        if (title === this.pages[i].title) {
            this.pages[i].active = true;
        }
    }
  }  
}
