// Treatment Model
export interface TreatmentModel {
	name:  		    string,
	experiences: 	number,
	favorite?:		boolean
}